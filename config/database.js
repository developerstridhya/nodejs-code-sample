// declare mongodb connection
const mongoose = require("mongoose");
mongoose.set("runValidators", true);
// dotenv configuration
const dotenv = require("dotenv");
dotenv.config();

mongoose
  .connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((data) =>
    console.log(`Mongodb connection established : ${data.connection.host}`)
  )
  .catch((err) => console.log("Mongodb Connection Error", err));
