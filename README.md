# NodeJS-Project-Structure

Clone this Repository

Run Following Command
- npm install

To run the server execute below command
- npm start

APIs Integrations and Endpoints

- User Sign Up - "POST" - "/api/v1/users/signup"
- User Login - "POST" - "/api/v1/users/login"
- Change Password - "POST" - "/api/v1/users/change-password"
- Forgot Password - "POST" - "/api/v1/users/forgot-password"
- Reset Password - "POST" - "/api/v1/users/reset-password"
- Initiate Subscription - "POST" - "/api/v1/subscription/subscribe"
- Payment Verification - "POST" - "/api/v1/subscription/paymentCallback"


DataBase -  MongoDB
Payment Integration - RazorPay