const route = require("express").Router();

const subscriptionController = require("../controllers/subscription.controller");
const { isAuthenticatedUser } = require("../middleware/authenticate");

route.post("/subscribe", isAuthenticatedUser, subscriptionController.subscribe);
route.post(
  "/paymentCallback",
  isAuthenticatedUser,
  subscriptionController.paymentCallback
);

module.exports = route;
