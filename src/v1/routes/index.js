const express = require("express");
const route = express.Router();

const userRoutes = require("./user.route");
const subscriptionRoutes = require("./subscription.route");

route.use("/v1/users", userRoutes);
route.use("/v1/subscription", subscriptionRoutes);

module.exports = route;
