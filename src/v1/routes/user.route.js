const route = require("express").Router();
const validatorFunc = require("../utils/errorValidation");
const {
  user_validator,
  login_validator,
} = require("../validator/user.validator");

const userController = require("../controllers/users.controller");
const { isAuthenticatedUser } = require("../middleware/authenticate");

route.post("/signUp", user_validator, validatorFunc, userController.signUp);
route.post("/login", login_validator, validatorFunc, userController.login);
route.post(
  "/change-password",
  isAuthenticatedUser,
  userController.changePassword
);
route.post("/forgot-password", userController.forgotPassword);
route.post("/reset-password", userController.resetPassword);

module.exports = route;
