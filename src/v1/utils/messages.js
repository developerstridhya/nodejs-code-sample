const EnglishData = require("../language/en.json");

exports.checkLanguage = (msg) => {
  return EnglishData[0][msg];
};
