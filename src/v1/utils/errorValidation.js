module.exports = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log("PASSWORD_VALIDATION....", errors.array()[0].msg);
    return sendResponseValidation(
      res,
      constants.WEB_STATUS_CODE.BAD_REQUEST,
      constants.STATUS_CODE.VALIDATION,
      errors.array()[0].msg,
      {},
      req.headers.lang,
      {}
    );
  }
  next();
};
