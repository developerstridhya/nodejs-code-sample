const { body, query } = require("express-validator");

//validate user form detail
exports.user_validator = [
  body("email")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.EMAIL_REQUIRED")
    .isEmail()
    .withMessage("USER_VALIDATION.VALID_EMAIL")
    .trim(),
  body("password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.PASSWORD_SIZE")
    .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{6,}$/)
    .withMessage("USER_VALIDATION.PASSWORD_VALIDATION"),
];

exports.login_validator = [
  body("email")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.EMAIL_REQUIRED")
    .isEmail()
    .withMessage("USER_VALIDATION.EMAIL_VALID")
    .trim(),
  body("password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.PASSWORD_REQUIRED")
    .trim(),
  body("platform_os")
    .optional()
    .isIn([1, 2])
    .withMessage("USER_VALIDATION.VALID_PLATFORM_OS"),
];

exports.changePassword_validator = [
  body("old_password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.OLD_PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.OLD_PASSWORD_SIZE"),
  body("new_password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.NEW_PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.NEW_PASSWORD_SIZE"),
  body("confirm_password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.CONFIRM_PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.CONFIRM_PASSWORD_SIZE"),
];

exports.resetPassword_validator = [
  body("new_password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.NEW_PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.NEW_PASSWORD_SIZE"),
  body("confirm_password")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.CONFIRM_PASSWORD_REQUIRED")
    .trim()
    .isLength({ min: 6 })
    .withMessage("USER_VALIDATION.CONFIRM_PASSWORD_SIZE"),
];

exports.forgotPassword_validator = [
  body("email")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.EMAIL_REQUIRED")
    .isEmail()
    .withMessage("USER_VALIDATION.VALID_EMAIL")
    .trim(),
];

exports.generateAuthToken_validator = [
  body("refresh_tokens")
    .not()
    .isEmpty()
    .withMessage("USER_VALIDATION.REFRESH_TOKENS_REQUIRED"),
];

exports.getUserList_validator = [
  query("user_type"),
  // .not()
  // .isEmpty()
  // .withMessage('USER_VALIDATION.USER_TYPE_REQUIRED')
  // .isIn([1, 2])
  // .withMessage('USER_VALIDATION.VALID_USER_TYPE')
];
