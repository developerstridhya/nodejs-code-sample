const catchAsyncError = require("../utils/catchAsyncError");
const jwt = require("jsonwebtoken");
const User = require("../models/user.model");
const ErrorHandler = require("../utils/errorHandler");

exports.isAuthenticatedUser = catchAsyncError(async (req, res, next) => {
  const token = req.headers.authorization;

  if (token) {
    var Authorization = token.split(" ")[1];
    const decodedUserData = jwt.verify(
      Authorization,
      process.env.JWT_SECRET_KEY
    );
    req.user = await User.findById(decodedUserData.id);
    next();
  } else {
    return next(new ErrorHandler("Please Login to access this resources", 200));
  }
});
