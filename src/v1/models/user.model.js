const mongoose = require("mongoose");
let userSchema = new mongoose.Schema(
  {
    first_name: {
      type: String,
      required: true,
    },
    last_name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true,
      lowercase: true,
    },
    address: {
      type: String,
    },
    contact_details: {
      type: Object,
    },
    password: {
      type: String,
      minlength: 6,
      required: true,
    },
    user_type: {
      type: String,
      default: "user",
    },
    payment_status: {
      type: Boolean,
      default: false,
    },
    passwordResetToken: {
      type: String,
    },
  },
  { timestamps: true }
);

let User = mongoose.model("users", userSchema);
module.exports = User;
