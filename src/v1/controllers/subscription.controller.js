const Razorpay = require("razorpay");
const catchAsyncError = require("../utils/catchAsyncError");
const sendResponse = require("../utils/sendResponse");
const ErrorHandler = require("../utils/errorHandler");
const { checkLanguage } = require("../utils/messages");

var razorpay = new Razorpay({
  key_id: process.env.RAZORPAY_KEY_ID || "rzp_test_UQvLCMy2pqehw8",
  key_secret: process.env.RAZORPAY_KEY_SECRET || "QqqHZs6Ndk39CNxjcfwTrAip",
});

exports.subscribe = catchAsyncError(async (req, res, next) => {
  const response = await razorpay.orders.create(options);

  return sendResponse(
    res,
    true,
    checkLanguage("orderCreated"),
    response.id,
    201
  );
});

exports.paymentCallback = catchAsyncError(async (req, res, next) => {
  const generated_signature = createHmac(
    "sha256",
    process.env.RAZORPAY_KEY_SECRET
  )
    .update(body.razorpay_order_id + "|" + body.razorpay_payment_id)
    .digest("hex");

  if (generated_signature != body.razorpay_signature) {
    return next(new ErrorHandler(checkLanguage("default"), 400));
  }
  await User.findByIdAndUpdate(req.user._id, { payment_status: true });
  return sendResponse(res, true, checkLanguage("purchased"), {}, 200);
});
