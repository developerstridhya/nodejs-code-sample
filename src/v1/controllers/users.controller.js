const {
  getUser,
  userCreate,
  JWTTokenCreation,
  forgotPassword,
  resetPassword,
} = require("../services/users.service");
const catchAsyncError = require("../utils/catchAsyncError");
const sendResponse = require("../utils/sendResponse");
const bcrypt = require("bcrypt");
const ErrorHandler = require("../utils/errorHandler");
const { checkLanguage } = require("../utils/messages");

exports.signUp = catchAsyncError(async (req, res, next) => {
  const reqBody = req.body;

  let existingUser = await getUser(reqBody.email, "email");

  if (existingUser)
    return next(new ErrorHandler(checkLanguage("userExist"), 400));

  reqBody.password = await bcrypt.hash(reqBody.password, 10);

  const user = await userCreate(reqBody);

  if (typeof user === "string")
    return next(new ErrorHandler(checkLanguage(user), 400));

  return sendResponse(res, true, checkLanguage("userCreated"), user, 201);
});

exports.login = catchAsyncError(async (req, res, next) => {
  const reqBody = req.body;

  let existingUser = await getUser(reqBody.email, "email");

  if (!existingUser)
    return next(new ErrorHandler(checkLanguage("userNotExist"), 400));

  const passwordCheck = bcrypt.compare(reqBody.password, existingUser.password);
  if (!passwordCheck) {
    return next(new ErrorHandler(checkLanguage("wrongCredentials"), 400));
  }
  const token = JWTTokenCreation(existingUser);
  return sendResponse(res, true, checkLanguage("loggedIn"), token, 201);
});

exports.changePassword = catchAsyncError(async (req, res, next) => {
  const reqBody = req.body;

  if (reqBody.new_password !== reqBody.confirm_password)
    return next(new ErrorHandler("New password Mismached!", 400));

  let userDetails = await getUser(req.user._id);

  const passwordCheck = bcrypt.compare(
    reqBody.old_password,
    existingUser.password
  );
  if (!passwordCheck)
    return next(new ErrorHandler(checkLanguage("oldIncorrectPass"), 400));

  if (reqBody.new_password === reqBody.old_password)
    return next(new ErrorHandler(checkLanguage("oldNewPassSame"), 400));

  userDetails.password = await bcrypt.hash(reqBody.new_password, 10);

  await userDetails.save();

  return sendResponse(
    res,
    true,
    checkLanguage("passChanged"),
    userDetails,
    200
  );
});

exports.forgotPassword = catchAsync(async (req, res, next) => {
  const reqData = {};
  reqData.email = req.body.email;

  const resMessage = await forgotPassword(reqData);

  if (typeof resMessage === "string")
    return next(new ErrorHandler(resMessage, 400));

  return sendResponse(res, true, checkLanguage("emailSent"), resMessage, 200);
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const reqData = {
    token: req.query.token,
    password: req.body.password,
  };

  const resMessage = await resetPassword(reqData);

  if (typeof resMessage === "string")
    return next(new ErrorHandler(resMessage, 400));

  return sendResponse(res, true, checkLanguage("passReset"), {}, 200);
});
