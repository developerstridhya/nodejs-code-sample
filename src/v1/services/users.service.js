const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const sendEmail = require("../utils/sendEmail");

exports.getUser = async (idOrEmail, fieldName = "_id") => {
  const data = await User.findOne({
    [fieldName]: `${idOrEmail}`,
  });
  return data;
};

exports.userCreate = async (userData) => {
  const user = await User.create(userData);

  if (user) return this.JWTTokenCreation(user);
  return "userCreationFailed";
};

exports.JWTTokenCreation = (userData) => {
  const id = userData._id;
  const token = jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });

  const data = {
    token,
    userData,
  };
  return data;
};

exports.forgotPassword = async (data) => {
  const user = await User.findOne({
    email: data.email,
  });

  if (!user) return "User Not Found!";

  const name = user.first_name;
  const resetToken = this.createPasswordResetToken();
  user.passwordResetToken = resetToken;

  await user.save();

  const link = `${process.env.WEBHOST}/auth/reset-password?token=${resetToken}&name=${name}`;
  const emailBody = `
  Hello ${name},
  Here is the Link of Forgot Password.
  ${link}
  `;

  try {
    sendEmail({
      email: user.email,
      subject: "Forgot Password",
      message: emailBody,
    });

    return user;
  } catch (err) {
    console.log(err);
    return "Error While Sending Mail!";
  }
};

exports.resetPassword = async (data) => {
  let response = {};

  const hashedToken = crypto
    .createHash("sha256")
    .update(data.token)
    .digest("hex");

  const user = await User.findOne({
    passwordResetToken: hashedToken,
  }).select("email password");

  if (!user) return "Invalid Token!";

  if (await user.correctPassword(data.password, user.password))
    return "Old and New Password are same!!";

  const hash = await bcrypt.hash(data.password, 10);

  user.password = hash;
  user.passwordResetToken = undefined;
  await user.save();

  response._id = user._id;
  response.email = user.email;

  return true;
};

exports.createPasswordResetToken = () => {
  return crypto.randomBytes(32).toString("hex");
};
