const express = require("express");
const app = express();
const logger = require("morgan");
const cors = require("cors");
require("dotenv").config();
require("./config/database");
const errorHandler = require("./src/v1/utils/error");
const routes = require("./src/v1/routes/index");
app.use(logger("dev"));
app.use(express.json());

process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION! Shutting down...");
  console.log(err.name, err.message, err);
  process.exit(1);
});

// provide cors options and configurations
const corsOptions = {
  origin: "*",
  credentials: true,
};
app.use(cors(corsOptions));

app.use("/api", routes);

app.use(errorHandler);
app.listen(process.env.PORT, () => {
  console.log(`server started at port : ${process.env.PORT}`);
});
